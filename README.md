# Mycroft Bigscreen Setup
Provides first boot mycroft configuration and setup support for Plasma Bigscreen

## About 
The skill provides configuration options for users to personalize their mycroft
experience on first boot. It also handles the pairing process for backend services
provided by Mycroft AI. Pairing a device with Home provides access
to privacy-protecting Speech to Text, Wolfram Alpha and other such services,
as well as easy configuration for all your Mycroft devices.

## Examples 
* "Pair my device" (happens automatically on first run if not paired already)

## Credits 
AIIX (@AIIX)
Mycroft AI (@MycroftAI)

## Category
**Configuration**

## Tags
#configuration
#setup
#first-boot
#pair
#pairing
#connectivity
#system
