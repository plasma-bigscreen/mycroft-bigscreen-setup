/*
 * Copyright 2018 Aditya Mehra <aix.m@outlook.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import QtQuick.Layouts 1.4
import QtQuick 2.4
import QtQuick.Controls 2.0
import org.kde.kirigami 2.5 as Kirigami
import org.kde.plasma.core 2.0 as PlasmaCore
import Mycroft 1.0 as Mycroft

Item {
    id: backendView
    anchors.fill: parent
    property int speakConfig: sessionData.firstConfigDialog
    property bool configurePtt: sessionData.configurePtt

    onConfigurePttChanged: {
        if(configurePtt){
            console.log("I should configure PTT")
        }
    }

    onSpeakConfigChanged: {
        if(speakConfig == 1){
            console.log("I am only 1")
            triggerGuiEvent("mycroft.device.speak.first.config", {})
        }
    }

    Component.onCompleted: {
        pos2.forceActiveFocus()
    }

    Row {
        anchors.fill: parent

        Rectangle {
            id: pos1
            color: Qt.rgba(0, 0, 0, 1)
            width: root.width / 3
            height: parent.height

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: Kirigami.Units.largeSpacing
                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        font.bold: true
                        minimumPixelSize: height * 0.15
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.25
                        text: "Welcome To Mycroft Setup"
                    }
                }

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.15
                        text: "This setup will take you through various steps to get your device ready"
                    }
                }

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.15
                        text: "Let's get started with configuring how you would like to interact with Mycroft"
                    }
                }

                Kirigami.Separator {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    color: "white"
                }

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignBottom

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.10
                        text: "Change options using ◄ and ► arrow keys, Select one to continue"
                    }
                }
            }
        }

        Rectangle {
            id: pos2
            color: "#45c5f7"
            width: root.width / 3
            height: parent.height
            focus: true
            border.width: pos2.activeFocus ? Kirigami.Units.smallSpacing : 0
            border.color: pos2.activeFocus ? "white" : "transparent"
            KeyNavigation.right: pos3

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: Kirigami.Units.largeSpacing

                Kirigami.Icon {
                    source: Qt.resolvedUrl("images/tv.svg")
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height / 4.5
                    color: "white"
                }
                Kirigami.Icon {
                    source: Qt.resolvedUrl("images/remote.svg")
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height / 4.5
                    color: "white"
                }
                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        horizontalAlignment: Text.AlignHCenter
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.25
                        text: "Push-To-Talk + Wakeword"
                    }
                }

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        horizontalAlignment: Text.AlignHCenter
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.15
                        text: "Allows you to activate mycroft with the push of a button in addition to the 'Hey Mycroft' wakeword"
                    }
                }

                Kirigami.Icon {
                    source: Qt.resolvedUrl("images/greentick.svg")
                    visible: pos2.activeFocus
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height * 0.08
                }
            }

            Keys.onReturnPressed: {
                triggerGuiEvent("mycroft.device.set.first.config", {"config": "pttwakeword"})
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    triggerGuiEvent("mycroft.device.set.first.config", {"config": "pttwakeword"})
                }
            }
        }

        Rectangle {
            id: pos3
            color: "#4577f7"
            width: root.width / 3
            height: parent.height
            border.width: pos3.activeFocus ? Kirigami.Units.smallSpacing : 0
            border.color: pos3.activeFocus ? "white" : "transparent"

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: Kirigami.Units.largeSpacing

                Kirigami.Icon {
                    source: Qt.resolvedUrl("images/tv.svg")
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height / 4.5
                    color: "white"
                }
                Kirigami.Icon {
                    source: Qt.resolvedUrl("images/micarray.svg")
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height / 4.5
                    color: "white"
                }
                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        horizontalAlignment: Text.AlignHCenter
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.25
                        text: "Wakeword Only"
                    }
                }

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        anchors.fill: parent
                        wrapMode: Text.WordWrap
                        color: "white"
                        minimumPixelSize: height * 0.05
                        horizontalAlignment: Text.AlignHCenter
                        fontSizeMode: Text.Fit
                        font.pixelSize: height * 0.15
                        text: "Allows you to activate mycroft with only 'Hey Mycroft' wakeword"
                    }
                }

                Kirigami.Icon {
                    source: Qt.resolvedUrl("images/greentick.svg")
                    visible: pos3.activeFocus
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height * 0.08
                }
            }

            Keys.onReturnPressed: {
                triggerGuiEvent("mycroft.device.set.first.config", {"config": "wakeword"})
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    triggerGuiEvent("mycroft.device.set.first.config", {"config": "wakeword"})
                }
            }
        }
    }
} 
